# Importing Libraries..
import os, subprocess, io, re
import logging
from flask import Flask, render_template, request, send_file, session, redirect, url_for
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
from wtforms import StringField, PasswordField, BooleanField
from wtforms import DecimalField, RadioField, SelectField, TextAreaField, FileField
from wtforms.validators import InputRequired
from werkzeug.security import generate_password_hash
import uuid
import tempfile

app = Flask(__name__)
app.config.from_prefixed_env()

# Replace the hardcoded secret key with an environment variable
app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET_KEY')
if not app.config['SECRET_KEY']:
    raise ValueError("No SECRET_KEY set for Flask application. This is insecure.")

class MyForm(FlaskForm):
  message = TextAreaField('Trades')
  excel = FileField()
  bittytax_debug = BooleanField('Debug (--debug)')
  bittytax_skipint = BooleanField('Skipint (--skipint)')
  bittytax_auditonly = BooleanField('Audit only (--audit)')
  bittytax_summary = BooleanField('Summary (--summary)')
  bittytax_nopdf = BooleanField('No PDF (--nopdf)', default=True)
  bittytax_taxyear = SelectField(u'Taxyear', choices=[('', 'All tax years'), ('2024', '2023/24'), ('2025', '2024/25')])
  bittytax_type = RadioField('Type', choices=[('shares', 'Stocks & Shares'), ('crypto', 'Crypto')], default='shares')
  bittytax_entity = SelectField('Entity', choices=[
        ('UK_INDIVIDUAL', 'UK Individual'),
        ('UK_COMPANY_JAN', 'UK Company (Jan FY start)'),
        ('UK_COMPANY_FEB', 'UK Company (Feb FY start)'),
        ('UK_COMPANY_MAR', 'UK Company (Mar FY start)'),
        ('UK_COMPANY_APR', 'UK Company (Apr FY start)'),
        ('UK_COMPANY_MAY', 'UK Company (May FY start)'),
        ('UK_COMPANY_JUN', 'UK Company (Jun FY start)'),
        ('UK_COMPANY_JUL', 'UK Company (Jul FY start)'),
        ('UK_COMPANY_AUG', 'UK Company (Aug FY start)'),
        ('UK_COMPANY_SEP', 'UK Company (Sep FY start)'),
        ('UK_COMPANY_OCT', 'UK Company (Oct FY start)'),
        ('UK_COMPANY_NOV', 'UK Company (Nov FY start)'),
        ('UK_COMPANY_DEC', 'UK Company (Dec FY start)')
    ], default='UK_INDIVIDUAL')
  


@app.route('/', methods=['GET', 'POST'])
def index():
  app.logger.debug(os.environ.get('FLASK_SECRET_KEY'))

  form = MyForm()
  if form.validate_on_submit():
    struuid = str(uuid.uuid4());
    
    if form.excel.data:
      # New file uploaded
      original_filename = secure_filename(form.excel.data.filename)
      temp_dir = tempfile.gettempdir()
      filepath = os.path.join(temp_dir, f"{struuid}_{original_filename}")
      form.excel.data.save(filepath)
      session['stored_file'] = {'path': filepath, 'name': original_filename}
      app.logger.debug(f"Stored new file: {original_filename}")
    elif 'stored_file' in session:
      # Use previously stored file
      filepath = session['stored_file']['path']
      original_filename = session['stored_file']['name']
      app.logger.debug(f"Using stored file: {original_filename}")
    else:
      # Use text input
      original_filename = f"{struuid}.csv"
      filepath = os.path.join(app.instance_path, original_filename)
      comma_separated_output = form.message.data.replace('\t', ',')

      
      with io.open(filepath, "w", encoding="utf-8") as file:
        file.write(f"{comma_separated_output}\n")
      app.logger.debug(f"Created new temporary file from text input")
    
    output, error = run_bittytax(filepath, form, struuid)  # Capture both output and error
    app.logger.debug(f"Bittytax output: {output[:5]}")  # Added logging for output, only first 5 lines
    app.logger.debug(f"Bittytax error: {error}")  # Added logging for error
    
    stored_filename = session.get('stored_file', {}).get('name')
    app.logger.debug(f"Stored filename after processing: {stored_filename}")
    
    return render_template('index.html', form=form, output=output, error=error, original_filename=original_filename, pdf=f"{struuid}.pdf", stored_filename=stored_filename)
  else:
    stored_filename = session.get('stored_file', {}).get('name')
    app.logger.debug(f"Stored filename on GET request: {stored_filename}")
    return render_template('index.html', form=form, stored_filename=stored_filename)

if __name__ == '__main__':
  app.run()

@app.route('/parse')
def parse():
  return run_bittytax()

def run_bittytax(filename, form, struuid):

    base_command = ["/usr/bin/sudo", "-i", "-u", "shares", "bittytax"]

    if form.bittytax_type.data == 'crypto':
      base_command = ["/usr/bin/sudo", "-i", "-u", "crypto", "bittytax"]

    # Check if variables are set and add corresponding arguments
    if form.bittytax_debug.data:
        base_command.append("--debug")
    if form.bittytax_skipint.data:
        base_command.append("--skipint")
    if form.bittytax_auditonly.data:
        base_command.append("--audit")
    if form.bittytax_summary.data:
        base_command.append("--summary")
    if form.bittytax_taxyear.data:
        base_command.append("-ty")
        base_command.append(form.bittytax_taxyear.data)
    
    if form.bittytax_entity.data != 'UK_INDIVIDUAL':
        base_command.append("--taxrules")
        base_command.append(form.bittytax_entity.data)

    if form.bittytax_nopdf.data:
        base_command.append("--nopdf")
    else:
      pdf_filename = f"{struuid}.pdf"
      pdf_filepath = os.path.join(app.instance_path, pdf_filename)
      base_command.append(f"-o{pdf_filepath}")

    # add filepath
    base_command.append(filename)

    process = subprocess.run(base_command, capture_output=True, text=True)
    output, error = process.stdout, process.stderr
    app.logger.debug(f"Running command: {' '.join(base_command)}")  # Added logging for command
    return output, error  # Return both output and error




@app.route('/download_file/<filename>')
def download_file(filename):
    # Check if the pre-existing file exists
    file_path = os.path.join(app.instance_path + "/" + filename)  # Adjust the path to your files directory
    if os.path.exists(file_path):
        # Serve the pre-existing file for download
        return send_file(file_path, as_attachment=True)
    else:
        return 'File not found', 404

@app.route('/clear_stored_file')
def clear_stored_file():
    if 'stored_file' in session:
        filepath = session['stored_file']['path']
        if os.path.exists(filepath):
            os.remove(filepath)
        session.pop('stored_file', None)
    return redirect(url_for('index'))

# Add this new route to check session data
@app.route('/check_session')
def check_session():
    stored_file = session.get('stored_file', {})
    return f"Stored file in session: {stored_file}"
