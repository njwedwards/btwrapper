# btwrapper

A web interface to BittyTax.

## To run locally on Linux

    python3 -m venv .venv # setup venv
    source .venv/bin/activate
    pip install -r requirements.txt # install requirements
    mkdir instance # for reports etc
    export FLASK_APP=index.py
    export FLASK_SECRET_KEY=<secretkey>
    export FLASK_DEBUG=1 # optional debug
    flask run --host=0.0.0.0 --port=5005 --reload --debug # run application in dev mode

The app is setup to run different instances of BittyTax (with different configs) depending on whether you are dealing with shares or crypto. You can just modify the command in code, but you will need to set things up separately.